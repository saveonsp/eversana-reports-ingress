from mcr.microsoft.com/mssql/server

user root
run mkdir /data && chown -R mssql /data

user mssql
volume /data
env ACCEPT_EULA=Y
env SA_PASSWORD=Password--
env MSSQL_TCP_PORT=1433


copy entrypoint.sh init.sql init.sh /tmp/mssql/
cmd ["/tmp/mssql/entrypoint.sh"]
