USE SaveonSP;
GO
DROP TABLE IF EXISTS EversanaClaims
GO
CREATE TABLE EversanaClaims (
  ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  date_processed datetime,
  date_of_fill datetime,
  claim_ref nvarchar(100),
  cardholder_group nvarchar(100),
  cardholder_id nvarchar(100),
  ndc nvarchar(11),
  label_name nvarchar(100),
  qty float,
  amount_paid float,
  pharmacy_number nvarchar(10),
  pharmacy_name nvarchar(100),
  pharmacy_city nvarchar(100),
  pharmacy_state nvarchar(2),
  pharmacy_zip nvarchar(5),
  provider_name nvarchar(100),
  provider_dea nvarchar(10),
  provider_city nvarchar(100),
  provider_state nvarchar(2),
  provider_zip nvarchar(5),
  source_file_name nvarchar(100),
  source_file_id nvarchar(100),
  source_file_row int
);
