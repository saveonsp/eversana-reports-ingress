import re
import csv
from datetime import datetime
from pathlib import Path
from pprint import pprint
from collections import defaultdict
from itertools import islice

import pymssql
from openpyxl import load_workbook


reg = re.compile("Save\sOn\sSP\sClaims\sDetail\s(?P<a>.+)\sv00\.xlsx")

root_path = Path.home() / "u" / "Accredo-ESI" / "NJM" / "Eversana Reports"


TABLE_NAME = "EversanaClaims"

COLUMNS = [
    "date_processed",
    "date_of_fill",
    "claim_ref",
    "cardholder_group",
    "cardholder_id",
    "ndc",
    "label_name",
    "qty",
    "amount_paid",
    "pharmacy_number",
    "pharmacy_name",
    "pharmacy_city",
    "pharmacy_state",
    "pharmacy_zip",
    "provider_name",
    "provider_dea",
    "provider_city",
    "provider_state",
    "provider_zip",
    "source_file_name",
    "source_file_id",
    "source_file_row",
]

COLUMN_TYPES = {
    "date_processed": (
        {
            datetime,
        },
        None,
    ),
    "date_of_fill": (
        {
            datetime,
        },
        None,
    ),
    "claim_ref": (
        {
            str,
        },
        None,
    ),
    "cardholder_group": (
        {
            str,
        },
        None,
    ),
    "cardholder_id": (
        {
            int,
            str,
        },
        lambda v: str(v) if type(v) is int else v,
    ),
    "ndc": (
        {
            str,
        },
        None,
    ),
    "label_name": (
        {
            str,
        },
        None,
    ),
    "qty": (
        {
            float,
            int,
        },
        None,
    ),
    "amount_paid": (
        {int, float},
        lambda v: float(v) if type(v) is int else v,
    ),
    "pharmacy_number": (
        {
            str,
        },
        None,
    ),
    "pharmacy_name": (
        {
            str,
        },
        None,
    ),
    "pharmacy_city": (
        {
            str,
        },
        None,
    ),
    "pharmacy_state": (
        {
            str,
        },
        None,
    ),
    "pharmacy_zip": (
        {
            str,
        },
        None,
    ),
    "provider_name": (
        {
            str,
        },
        None,
    ),
    "provider_dea": (
        {
            int,
            str,
        },
        lambda d: str(d) if type(d) is int else d,
    ),
    "provider_city": (
        {
            str,
        },
        None,
    ),
    "provider_state": (
        {
            str,
        },
        None,
    ),
    "provider_zip": (
        {
            int,
            str,
        },
        lambda d: str(d).rjust(5, "0") if type(d) is int else d,
    ),
    "source_file_name": (
        {
            str,
        },
        None,
    ),
    "source_file_id": (
        {
            str,
        },
        None,
    ),
    "source_file_row": (
        {
            int,
        },
        None,
    ),
}

header_map = {
    "CLAIM DETAIL": "date_processed",
    "DATE OF FILL": "date_of_fill",
    "DATE PROCESSED": "date_processed",
    "CLAIM REFERENCE NUMBER": "claim_ref",
    "GROUP #": "cardholder_group",
    "CARD ID #": "cardholder_id",
    "NDC": "ndc",
    "LABEL NAME": "label_name",
    "QTY VL": "qty",
    "TOTAL AMOUNT PAID": "amount_paid",
    "PHARMACY #": "pharmacy_number",
    "PHARMACY NAME": "pharmacy_name",
    "CITY": "city",
    "STATE": "state",
    "ZIPCODE": "zip",
    "PROVIDER NAME": "provider_name",
    "DEA # / NPI": "provider_dea",
}

xlsx_files = sorted(p for p in root_path.glob("**/[!~]*.xlsx"))

conn = pymssql.connect(
    #    server="wamp",
    #    user="user",
    #    password="password",
    #    database="SaveonSP",
    #    port=1433,
)
cursor = conn.cursor()
cursor.execute(f"TRUNCATE TABLE {TABLE_NAME}")

records = []
for path in xlsx_files:
    wb = load_workbook(filename=path, read_only=True, data_only=True)
    ws = wb["Details"]

    _id = reg.match(path.name).groups()[0]

    header = [ws.cell(column=j + 1, row=2).value for j in range(ws.max_column)]

    after_provider = False

    header_keys = {}
    for j, v in enumerate(header):
        k = header_map[v]
        if k == "provider_name":
            after_provider = True

        if k in ("city", "state", "zip"):
            prefix = "provider" if after_provider else "pharmacy"
            k = f"{prefix}_{k}"

        header_keys[j] = k

    min_row = 4
    for i, row in enumerate(ws.iter_rows(min_row=min_row)):
        record = {
            "source_file_name": path.name,
            "source_file_id": _id,
            "source_file_row": i + min_row,
        }
        for j, k in header_keys.items():
            t, fn = COLUMN_TYPES[k]
            v = row[j].value
            if v is None:
                continue
            assert (
                type(v) in t
            ), f"unexpected cell type for column {k} {type(v)} not in {t}"

            if fn is not None:
                v = fn(v)

            if type(v) is str:
                v = v.strip()

            record[k] = v

        records.append(record)


q = ",".join(["%s"] * len(COLUMNS))
cursor.executemany(
    f"insert into EversanaClaims values ({q})",
    [tuple(o.get(k) for k in COLUMNS) for o in records],
)
conn.commit()
