import pymssql

print("connecting...")
with pymssql.connect(
    server="localhost",
    user="testuser",
    password="Password--",
    database="SaveonSP",
    port=1433,
) as conn:
    with conn.cursor() as cursor:
        query = """
        select
          source_file_id,
          count(*)
        from EversanaClaims
        group by source_file_id
        order by source_file_id
        """
        cursor.execute(query)
        for row in cursor:
            print(row)
