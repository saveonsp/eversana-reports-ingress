USE master ;
GO
CREATE DATABASE SaveonSP;
-- ON (
--   NAME = SaveonSP_dat,
--   FILENAME = '/data/SaveonSP_dat.mdf',
--   SIZE = 1000MB,
--   MAXSIZE = 50MB,
--   FILEGROWTH = 5MB)
-- LOG ON (
--   NAME = SaveonSP_log,
--   FILENAME = '/data/SaveonSP_log.ldf',
--   SIZE = 1000MB,
--   MAXSIZE = 25MB,
--   FILEGROWTH = 5MB);
GO

USE SaveonSP;
GO
CREATE LOGIN testuser WITH PASSWORD = 'Password--';
GO
CREATE USER testuser FOR LOGIN testuser;
GO
ALTER ROLE db_owner ADD MEMBER testuser;
GO
