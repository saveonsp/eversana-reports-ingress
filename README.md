# Eversana Reports Database Ingress

create table & import eversana reports


## Instructions
1. setup python env `python3 -m venv env` and `source env/bin/activate`  
3. install python deps `python3 -m pip install -r requirements.txt`  
4. start database `docker-compose build && docker-compose up -d`   
5. create table `./create-table.sh`  
6. import data `python3 main.py`  
