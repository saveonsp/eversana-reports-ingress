-- find reversals? (duplicate claim_refs)
select b.*
from (
	select claim_ref
	from EversanaClaims e
	where claim_ref is not null
	group by claim_ref
	having count(*) > 1
) a
join EversanaClaims b on a.claim_ref = b.claim_ref
order by claim_ref desc
