#!/bin/bash
docker run \
  --network=host \
  --rm \
  --user root \
  -v $(pwd)/create-table.sql:/tmp/create-table.sql \
  mcr.microsoft.com/mssql/server \
  /opt/mssql-tools/bin/sqlcmd \
  -S localhost \
  -U testuser \
  -P Password-- \
  -i /tmp/create-table.sql
